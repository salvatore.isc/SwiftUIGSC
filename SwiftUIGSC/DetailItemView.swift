//
//  DetailItemView.swift
//  SwiftUIGSC
//
//  Created by Salvador Lopez on 16/06/23.
//

import SwiftUI

struct DetailItemView: View{
    var item: Item
    var body: some View{
        VStack{
            Text(item.name)
                .font(.largeTitle)
            Text(item.description)
                .font(.headline)
            Text("\(item.id)")
                .font(.subheadline)
        }
    }
}

struct DetailItemView_Previews: PreviewProvider {
    static var previews: some View {
        DetailItemView(item: Item(name: "Test", description: "Test"))
    }
}
