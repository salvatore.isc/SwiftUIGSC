//
//  SwiftUIGSCApp.swift
//  SwiftUIGSC
//
//  Created by Salvador Lopez on 16/06/23.
//

import SwiftUI

@main
struct SwiftUIGSCApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView2()
        }
    }
}
