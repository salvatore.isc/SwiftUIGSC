//
//  Item.swift
//  SwiftUIGSC
//
//  Created by Salvador Lopez on 16/06/23.
//

import Foundation

struct Item: Identifiable{
    var id = UUID()
    let name: String
    var description: String
}
