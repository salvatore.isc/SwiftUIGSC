//
//  ContentView.swift
//  SwiftUIGSC
//
//  Created by Salvador Lopez on 16/06/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
        }
        .padding()
    }
}

struct ContentView2: View {
    
    @State var texto: String = ""
    @State var pass: String = ""
    @State var politicaUso: Bool = false
    
    var body: some View{
        VStack{
            Text("¡Wecolme!")
                .font(.largeTitle)
                .fontWeight(.ultraLight)
                .foregroundColor(.mint)
            ZStack{
                Color.mint
                    .frame(width: 210, height: 210)
                    .cornerRadius(105)
                Image("tux")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200,height: 200)
                    .background(Color.red)
                    .cornerRadius(100)
            }
            VStack{
                Text("Login")
                    .font(.headline)
                    .fontWeight(.ultraLight)
                    .foregroundStyle(.gray)
                TextField("Ingresa un nombre de usuario", text: $texto)
                    .textFieldStyle(.roundedBorder)
                TextField("Ingresa una contraseña", text: $pass)
                    .textFieldStyle(.roundedBorder)
            }
            .padding()
            Button {
                print("Usuario: \(texto)")
                print("Contraseña: \(pass)")
                print(politicaUso ? "Acepto la politica" : "No acepto la politica")
            } label: {
                Text("Login")
                    .frame(width: 200, alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.mint)
                    .cornerRadius(30)
                
            }
            HStack{
                Button("Registro"){
                    print("Registrar...")
                }
                .foregroundColor(.mint)
                Button("| Olvide mi contraseña"){
                    print("Acciones pass...")
                }
                .foregroundColor(.mint)
            }
            HStack{
                Toggle(isOn: $politicaUso) {
                    Text("Politica de privacidad:")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                .padding()
            }
        }
    }
}

struct SliderContentView: View{
    @State var fontSize: CGFloat = 20
    var body: some View{
        VStack{
            Text("¡Cambia el tamaño de la fuente!")
                .font(.system(size: fontSize))
                .padding()
            Slider(value: $fontSize, in: 10...50, step: 1)
                .padding()
        }
    }
}

struct StepperContentView:View{
    @State var itemCount: Int = 0
    var body: some View{
        VStack{
            Text("Carrito de compra")
                .font(.title)
                .padding(.top, 20)
            Spacer()
            Image(systemName: "cart")
                .resizable()
                .frame(width: 100,height: 100,alignment:.center)
            Spacer()
            Text("Cantidad: \(itemCount)")
                .font(.headline)
            Stepper {
                Text("Agregar al carrito")
            } onIncrement: {
                self.itemCount += 1
            } onDecrement: {
                if itemCount > 0 {
                    self.itemCount -= 1
                }
            }
            .padding()
            
            Button {
                print("Continuar con la compra...")
            } label: {
                Text("Comprar")
                    .foregroundColor(Color.white)

            }
            .padding()
            .background(Color.mint)
            .cornerRadius(10)
            Spacer()


        }
    }
}

struct ListContentView: View{
    
    var items = [
        Item(name: "Articulo 1", description: "Descripcion 1"),
        Item(name: "Articulo 2", description: "Descripcion 2"),
        Item(name: "Articulo 3", description: "Descripcion 3"),
        Item(name: "Articulo 4", description: "Descripcion 4"),
        Item(name: "Articulo 5", description: "Descripcion 5"),
        Item(name: "Articulo 6", description: "Descripcion 6"),
        Item(name: "Articulo 7", description: "Descripcion 7"),
        Item(name: "Articulo 8", description: "Descripcion 8"),
        Item(name: "Articulo 9", description: "Descripcion 9")
    ]
    
    var body: some View{
        NavigationView{
            List(items) {
                item in
                NavigationLink(destination: DetailItemView(item:item)) {
                    Image(systemName: "cart.circle.fill")
                    Text(item.name)
                }
            }
        }
    }
}

struct ScrollContentView: View{
    var body: some View{
        ScrollView([.horizontal,.vertical]){
            Image("colorful")
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct TabContentView: View{
    @State var selectedTab = 1
    var body: some View{
        TabView(selection: $selectedTab) {
            Image("tux")
                .resizable()
                .scaledToFit()
                .tag(0)
                .tabItem {
                    Label("Tab 1", systemImage: "house")
                }
            VStack{
                Circle()
                    .foregroundColor(.blue)
                    .frame(width: 100, height: 100)
                Text("Este es el tab 2")
            }
            .tag(1)
            .tabItem {
                Label("Tab 2", systemImage: "heart")
            }
            Text("This is tab 3")
                .tag(2)
                .tabItem {
                    Label("Tab 3", systemImage: "map")
                }
        }
    }
}

struct AlertContentView: View{
    @State var showAlert = false

    var body: some View{
        VStack{
            Button("Mostar alerta") {
                self.showAlert = true
            }
        }
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Titulo de la alerta"),
                  message: Text("Aqui va el mensaje de la alerta"),
                  primaryButton: .default(
                    Text("Aceptar"),
                    action: {
                        print("Aceptado...")
                    }),
                  secondaryButton: .default(Text("Cancelar")))
        }
    }
}

struct AlertSheetContentView: View{
    @State var showAlert = false
    var body: some View{
        VStack{
            Button("Mostrar action sheet"){
                self.showAlert = true
            }
        }
        .actionSheet(isPresented: $showAlert) {
            ActionSheet(
                title:
                    Text("Selecciona una opcion..."),
                message: Text("Aqui va el mensaje principal del action sheet"),
                buttons: [
                    .default(Text("Opcion 1")){
                        print("uno")
                    },
                    .default(Text("Option 2")){
                        print("dos")
                    },
                    .cancel(Text("Cancelar"))
                ])
        }
    }
}

struct DateContentView:View{
    @State var selectedDate = Date()
    var body: some View{
        VStack{
            DatePicker("Selecciona una fecha", selection: $selectedDate, displayedComponents: [.date])
                .datePickerStyle(.wheel)
                .labelsHidden()
                .onChange(of: selectedDate) { newValue in
                    print(newValue)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/YY"
                    print(formatter.string(from: newValue))
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        DateContentView()
    }
}
